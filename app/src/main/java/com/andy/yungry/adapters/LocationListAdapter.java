package com.andy.yungry.adapters;

import android.location.Address;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.andy.yungry.R;
import com.andy.yungry.events.location.OpenLocationEvent;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class LocationListAdapter extends RecyclerView.Adapter<LocationListAdapter.ViewHolder> {

    public List<String> address;


    public LocationListAdapter(List<String> address) {
        this.address = address;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_location, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        final String addressLn = address.get(position);

        holder.tvAddressline1.setText(addressLn);
        //holder.tvAddressline2.setText(getLine(addressLn,1));

        holder.item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EventBus.getDefault().post(new OpenLocationEvent(addressLn));
            }
        });

    }

    public String getLine(String address, int line) {

        int counter = 0;
        for( int i=0; i<address.length(); i++ ) {
            if( address.charAt(i) == ',' ) {
                counter++;
            }
        }

        if(counter == 1) {
            return address.split("," , 0)[line];
        } else {
            return address.split("," , (counter / 2) - 1)[line];
        }
    }

    @Override
    public int getItemCount() {
        return address.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.iv_locationicon)
        ImageView ivLocationicon;
        @BindView(R.id.tv_addressline1)
        TextView tvAddressline1;
        @BindView(R.id.tv_addressline2)
        TextView tvAddressline2;

        View item;

        public ViewHolder(View itemView) {
            super(itemView);
            item = itemView;
            ButterKnife.bind(this,itemView);
        }
    }

}

package com.andy.yungry.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.andy.yungry.ui.fragments.AccountFragment;
import com.andy.yungry.ui.fragments.CartFragment;
import com.andy.yungry.ui.fragments.MenuFragment;
import com.andy.yungry.ui.fragments.OffersFragment;

/**
 * Created by tallesborges on 18/06/17.
 */

public class MainPagerAdapter extends FragmentPagerAdapter {

    public MenuFragment menuFragment;
    public CartFragment cartFragment;
    public OffersFragment offersFragment;
    public AccountFragment accountFragment;

    public MainPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
                if(menuFragment == null) {
                    menuFragment = new MenuFragment();
                }
                return menuFragment;
            case 1:
                if(cartFragment == null) {
                    cartFragment = new CartFragment();
                }
                return cartFragment;
            case 2:
                if(offersFragment == null) {
                    offersFragment = new OffersFragment();
                }
                return offersFragment;
            default:
                if(accountFragment == null) {
                    accountFragment = new AccountFragment();
                }
                return accountFragment;
        }

    }

    @Override
    public int getCount() {
        // Show 3 total pages.
        return 4;
    }

}

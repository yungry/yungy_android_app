package com.andy.yungry.managers;

import android.location.Address;
import android.location.Location;

import com.google.android.gms.maps.model.LatLng;

public class UserLocationManager {

    public String deliveryLocation;

    public LatLng defaultLoaction = new LatLng(-33.8523341, 151.2106085);

    public Location userLocation;

    public Address userAddress;

    private static final UserLocationManager ourInstance = new UserLocationManager();

    public static UserLocationManager getInstance() {
        return ourInstance;
    }

    private UserLocationManager() {
    }

    public String getDeliveryLocation() {
        return deliveryLocation;
    }

    public void setDeliveryLocation(String deliveryLocation) {
        this.deliveryLocation = deliveryLocation;
    }

    public Address getUserAddress() {
        return userAddress;
    }

    public void setUserAddress(Address userAddress) {
        this.userAddress = userAddress;
    }

    public Location getUserLocation() {
        return userLocation;
    }

    public void setUserLocation(Location userLocation) {
        this.userLocation = userLocation;
    }
}

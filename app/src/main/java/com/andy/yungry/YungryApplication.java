package com.andy.yungry;

import android.app.Application;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

public class YungryApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();


        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/FuturaBookBT.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build());

    }

}

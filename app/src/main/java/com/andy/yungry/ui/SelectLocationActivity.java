package com.andy.yungry.ui;

import android.Manifest;
import android.app.FragmentTransaction;
import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.OrientationHelper;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.andy.yungry.R;
import com.andy.yungry.adapters.LocationListAdapter;
import com.andy.yungry.events.location.OpenLocationEvent;
import com.andy.yungry.events.location.UpdateLocationEvent;
import com.andy.yungry.managers.UserLocationManager;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.AutocompleteFilter;
import com.google.android.gms.location.places.AutocompletePrediction;
import com.google.android.gms.location.places.AutocompletePredictionBufferResponse;
import com.google.android.gms.location.places.GeoDataClient;
import com.google.android.gms.location.places.PlaceDetectionClient;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import permissions.dispatcher.NeedsPermission;
import permissions.dispatcher.OnPermissionDenied;
import permissions.dispatcher.RuntimePermissions;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

@RuntimePermissions
public class SelectLocationActivity extends AppCompatActivity implements OnMapReadyCallback, TextWatcher{


    private static final String TAG = "LOCATION ACTIVITY";

    private static final int DEFAULT_ZOOM = 18;

    @BindView(R.id.map_container)
    FrameLayout mapContainer;

    @BindView(R.id.rv_places)
    RecyclerView rvPlaces;

    @BindView(R.id.edittext_searchplace)
    EditText edittextSearchplace;

    @BindView(R.id.ll_getlocation)
    RelativeLayout btnGetlocation;

    @BindView(R.id.tv_cancel)
    TextView btnCancel;

    @BindView(R.id.tv_savelocation)
    TextView btnSavelocation;

    Handler handler;

    long delay = 500;
    long last_text_edit = 0;

    LocationListAdapter listAdapter;

    List<String> addressList;

    MapFragment mMapFragment;
    GoogleMap googleMap;

    private LatLng CBE_NORTH_EAST = new LatLng(11.096331, 77.019113);
    private LatLng CBE_SOUTH_WEST = new LatLng(10.964872, 76.987654);

    private LatLngBounds YUNGRY_SERVICE_BOUNDS;

    GoogleApiClient googleApiClient;

    private FusedLocationProviderClient mFusedLocationProviderClient;
    private GeoDataClient mGeoDataClient;
    private PlaceDetectionClient mPlaceDetectionClient;
    private Address mLastKnownAddress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_location);
        ButterKnife.bind(this);

        mFusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);
        mGeoDataClient = Places.getGeoDataClient(this, null);
        mPlaceDetectionClient = Places.getPlaceDetectionClient(this, null);

        buildYungryBoundries();

        edittextSearchplace.addTextChangedListener(this);

        handler = new Handler();

        rvPlaces.setLayoutManager(new LinearLayoutManager(this, OrientationHelper.VERTICAL,false));
        addressList = new ArrayList<>();
        listAdapter = new LocationListAdapter(addressList);
        rvPlaces.setAdapter(listAdapter);

    }

    @Override
    protected void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    private void buildYungryBoundries() {
        YUNGRY_SERVICE_BOUNDS = new LatLngBounds(CBE_SOUTH_WEST,CBE_NORTH_EAST);
    }

    @OnClick(R.id.ll_getlocation)
    public void clickGetLocation() {
        SelectLocationActivityPermissionsDispatcher.needLocationPermissionWithPermissionCheck(this);
    }

    public void initDeviceLocation() {
        googleMap = null;
        mMapFragment = MapFragment.newInstance();
        FragmentTransaction fragmentTransaction =
                getFragmentManager().beginTransaction();
        fragmentTransaction.add(R.id.map_container, mMapFragment);
        fragmentTransaction.commit();
        mMapFragment.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap map) {
                googleMap = map;
                try {
                    googleMap.setMyLocationEnabled(true);
                    googleMap.getUiSettings().setMyLocationButtonEnabled(false);
                    getDeviceLocation();
                } catch (SecurityException e ) {
                    Log.d(TAG, "onMapReady: permmsion not granted for location");
                }
            }
        });
    }

    public void initSelectedLocation(final String placeString) {
        googleMap = null;
        mMapFragment = MapFragment.newInstance();
        FragmentTransaction fragmentTransaction =
                getFragmentManager().beginTransaction();
        fragmentTransaction.add(R.id.map_container, mMapFragment);
        fragmentTransaction.commit();
        mMapFragment.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap map) {
                googleMap = map;
                try {
                    googleMap.setMyLocationEnabled(false);
                    googleMap.getUiSettings().setMyLocationButtonEnabled(false);

                    Address address = getAddress(placeString);

                    if(address == null) {
                        return;
                    }

                    edittextSearchplace.setText(address.getAddressLine(0));

                    LatLng latLng = new LatLng(address.getLatitude()
                            , address.getLongitude());

                    googleMap.addMarker(new MarkerOptions().position(latLng).title("Delivery Location"));
                    googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, DEFAULT_ZOOM));
                    googleMap.animateCamera(CameraUpdateFactory.zoomIn());
                    googleMap.animateCamera(CameraUpdateFactory.zoomTo(18), 500, null);


                } catch (SecurityException e ) {
                    Log.d(TAG, "onMapReady: permmsion not granted for location");
                }
            }
        });
    }

    @Override
    public void onMapReady(GoogleMap map) {

    }

    public void getDeviceLocation() {
        try {
            Task locationResult = mFusedLocationProviderClient.getLastLocation();
            locationResult.addOnCompleteListener(this, new OnCompleteListener() {
                @Override
                public void onComplete(@NonNull Task task) {
                    if (task.isSuccessful()) {
                        // Set the map's camera position to the current location of the device.

                        Location deviceLoc = (Location)task.getResult();
                         // set address in edit text

                        String address = getAddress(deviceLoc).getAddressLine(0);
                        if(address != null) {
                            edittextSearchplace.setText(address);
                        }

                        LatLng latLng = new LatLng(deviceLoc.getLatitude()
                                , deviceLoc.getLongitude());

                        googleMap.addMarker(new MarkerOptions().position(latLng).title("Delivery Location"));
                        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, DEFAULT_ZOOM));
                        googleMap.animateCamera(CameraUpdateFactory.zoomIn());
                        googleMap.animateCamera(CameraUpdateFactory.zoomTo(18), 500, null);

                    } else {
                        Log.d(TAG, "Current location is null. Using defaults.");
                        Log.e(TAG, "Exception: %s", task.getException());
                        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(UserLocationManager.getInstance().defaultLoaction, DEFAULT_ZOOM));
                        googleMap.getUiSettings().setMyLocationButtonEnabled(false);
                    }
                }
            });
        } catch (SecurityException e) {
            e.printStackTrace();
        }
    }

    public void getSuggestions(String place) {

        addressList.clear();

        AutocompleteFilter typeFilter = new AutocompleteFilter.Builder()
                .setCountry("IN")
                .setTypeFilter(AutocompleteFilter.TYPE_FILTER_NONE)
                .build();

        try {
            Task<AutocompletePredictionBufferResponse> predictionResult = mGeoDataClient.getAutocompletePredictions(place, YUNGRY_SERVICE_BOUNDS,typeFilter);

            predictionResult.addOnSuccessListener(this, new OnSuccessListener<AutocompletePredictionBufferResponse>() {
                @Override
                public void onSuccess(AutocompletePredictionBufferResponse autocompletePredictions) {
                    Iterator<AutocompletePrediction> iterator = autocompletePredictions.iterator();
                    if (iterator.hasNext()) {
                        while (iterator.hasNext()) {
                            AutocompletePrediction prediction = iterator.next();
                            String address = prediction.getFullText(null).toString();
                            addressList.add(address);
                        }
                    }else {
                        addressList.clear();
                    }
                    autocompletePredictions.release();
                    listAdapter.notifyDataSetChanged();
                }
            });

        } catch (SecurityException e) {

        }
    }

    public Address getAddress(Location location) {
        Geocoder geocoder = new Geocoder(this, Locale.getDefault());
        try {
            List<Address> address = geocoder.getFromLocation( location.getLatitude(), location.getLongitude(), 1);

            if (address.size() > 0) {
                mLastKnownAddress = address.get(0);
                return address.get(0);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public Address getAddress(String  placeString) {
        Geocoder geocoder = new Geocoder(this, Locale.getDefault());
        try {
            List<Address> address = geocoder.getFromLocationName(placeString, 1);

            if (address.size() > 0) {
                mLastKnownAddress = address.get(0);
                return address.get(0);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    /*public void searchLocation(String place) {
        addressList.clear();
        addressList.addAll(getAddressList(place));
        listAdapter.notifyDataSetChanged();
    }

    public List<Address> getAddressList (String place) {

        Geocoder geocoder = new Geocoder(this);
        try {
            List<Address> address = geocoder.getFromLocationName(place,20);
            Log.d(TAG, "getAddressList: lenght" + address.size());
            return address;
        } catch (IOException e) {
            e.printStackTrace();
        }

        return new ArrayList<Address>();
    }*/

    @OnClick(R.id.tv_savelocation)
    public void clickSaveLocation () {
        EventBus.getDefault().post(new UpdateLocationEvent(mLastKnownAddress));
        finish();
    }

    @OnClick(R.id.tv_cancel)
    public void clickCancel () {
        finish();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @NeedsPermission({Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION})
    void needLocationPermission() {
        initDeviceLocation();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        SelectLocationActivityPermissionsDispatcher.onRequestPermissionsResult(this, requestCode, grantResults);
    }

    @OnPermissionDenied({Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION})
    void onPermissionDenied() {
        Toast.makeText(this,"Need Permission to get you location", Toast.LENGTH_LONG).show();
        finish();
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        handler.removeCallbacks(searchLoaction);
    }

    @Override
    public void afterTextChanged(Editable s) {

        if (s.length() > 0) {
            last_text_edit = System.currentTimeMillis();
            handler.postDelayed(searchLoaction, delay);
        } else {
            handler.removeCallbacks(searchLoaction);
            addressList.clear();
            listAdapter.notifyDataSetChanged();
        }

    }

    private Runnable searchLoaction = new Runnable() {
        @Override
        public void run() {
            if (System.currentTimeMillis() > (last_text_edit + delay - 500)) {

                getSuggestions(edittextSearchplace.getText().toString());
            }
        }
    };

    ////////////////
    // EVENT ///////
    ////////////////

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvnet(OpenLocationEvent event) {
        initSelectedLocation(event.location);
    }
}

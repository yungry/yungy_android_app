package com.andy.yungry.ui;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageButton;

import com.andy.yungry.R;
import com.andy.yungry.adapters.MainPagerAdapter;
import com.andy.yungry.events.TabSelectedEvent;
import com.andy.yungry.views.CustomViewPager;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.ib_tab_item1)
    ImageButton ib_tab_item1;

    @BindView(R.id.ib_tab_item2)
    ImageButton ib_tab_item2;

    @BindView(R.id.ib_tab_item3)
    ImageButton ib_tab_item3;

    @BindView(R.id.ib_tab_item4)
    ImageButton ib_tab_item4;

    @BindView(R.id.vp_main)
    CustomViewPager viewPager;

    Unbinder unbinder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        unbinder = ButterKnife.bind(this);

        initAdapetes();

        onClickTabItems(0);
        EventBus.getDefault().register(this);
        EventBus.getDefault().post(new TabSelectedEvent(0));
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
        EventBus.getDefault().unregister(this);
    }

    private void initAdapetes() {
        MainPagerAdapter mAdapter = new MainPagerAdapter(getSupportFragmentManager());
        viewPager.setAdapter(mAdapter);
        viewPager.setPagingEnabled(false);
    }

    @OnClick(R.id.ib_tab_item1)
    void clickTab1() {
        onClickTabItems(0);
    }

    @OnClick(R.id.ib_tab_item2)
    void clickTab2() {
        onClickTabItems(1);
    }

    @OnClick(R.id.ib_tab_item3)
    void clickTab3() {
        onClickTabItems(2);
    }

    @OnClick(R.id.ib_tab_item4)
    void clickTab4() {
        onClickTabItems(3);
    }

    private void onClickTabItems(int item) {

        ib_tab_item1.setImageResource(R.drawable.ic_menu_off);
        ib_tab_item2.setImageResource(R.drawable.ic_cart_off);
        ib_tab_item3.setImageResource(R.drawable.ic_offers_off);
        ib_tab_item4.setImageResource(R.drawable.ic_account_off);

        switch (item) {
            case 0:
                ib_tab_item1.setImageResource(R.drawable.ic_menu_on);
                break;
            case 1:
                ib_tab_item2.setImageResource(R.drawable.ic_cart_on);
                break;
            case 2:
                ib_tab_item3.setImageResource(R.drawable.ic_offers_on);
                break;
            case 3:
                ib_tab_item4.setImageResource(R.drawable.ic_account_on);
                break;
        }

        EventBus.getDefault().post(new TabSelectedEvent(item));

    }


    ///////////////////
    /// EVNETS ////////
    ///////////////////

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onTabSelected(TabSelectedEvent event) {
        viewPager.setCurrentItem(event.position, false);
    }



    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

}

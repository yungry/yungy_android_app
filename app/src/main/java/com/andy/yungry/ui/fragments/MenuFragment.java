package com.andy.yungry.ui.fragments;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.andy.yungry.R;
import com.andy.yungry.events.location.UpdateLocationEvent;
import com.andy.yungry.managers.UserLocationManager;
import com.andy.yungry.ui.SelectLocationActivity;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * A simple {@link Fragment} subclass.
 */
public class MenuFragment extends Fragment {

    @BindView(R.id.tv_title)
    TextView title;

    @BindView(R.id.tv_deliverylocation)
    TextView textLocation;

    Unbinder unbinder;

    public MenuFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_menu, container, false);

        unbinder =  ButterKnife.bind(this,v);
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }

        updateLocation();

        return v;
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
        EventBus.getDefault().unregister(this);
    }


    @OnClick(R.id.ib_location)
    void clickLocation() {
        Intent intent = new Intent(getActivity(), SelectLocationActivity.class);
        getActivity().startActivity(intent);
    }

    public void updateLocation() {
        if(UserLocationManager.getInstance().getUserAddress() != null) {
            String deliveryLocation = UserLocationManager.getInstance().getUserAddress().getAddressLine(0);
            textLocation.setText(deliveryLocation);
        } else {
            textLocation.setText("Select Delivery Location");
        }
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(UpdateLocationEvent event) {
        UserLocationManager.getInstance().setUserAddress(event.address);
        updateLocation();
    }

}

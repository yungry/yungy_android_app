package com.andy.yungry.events.location;

public class OpenLocationEvent {
    public String location;

    public OpenLocationEvent(String location) {
        this.location = location;
    }
}

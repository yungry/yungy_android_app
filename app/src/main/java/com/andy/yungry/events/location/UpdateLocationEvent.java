package com.andy.yungry.events.location;

import android.location.Address;
import android.location.Location;

public class UpdateLocationEvent {

    public Location location;
    public Address address;

    public UpdateLocationEvent(Location location, Address address) {
        this.location = location;
        this.address = address;
    }

    public UpdateLocationEvent(Address address) {
        this.address = address;
    }
}

package com.andy.yungry.events;

public class TabSelectedEvent {
    public int position;

    public TabSelectedEvent(int position) {
        this.position = position;
    }
}
